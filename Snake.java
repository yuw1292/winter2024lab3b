import java.util.Scanner;

public class Snake {
	
	public String color; 
	public int age;
	public Boolean venomous; 
	
	public void killHumans() {
		if(this.venomous) {
			System.out.println("Your pet snake has bitten you, vennom is spreading throughout your body.");
		} else {
			System.out.println("Your pet snake is giving you small kisses :)");
		}
	}
	public void Wiggling() {
		if(this.age > 30) {
			System.out.println("Snake is too old to wiggle....");
		} else {
			System.out.println("Look at him go!");
		}
	}
}